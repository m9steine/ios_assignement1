//
//  ViewController.swift
//  Assignement1_Layout
//
//  Created by Martin Steiner on 15.12.18.
//  Copyright © 2018 Martin Steiner. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIGestureRecognizerDelegate{
    
    @IBOutlet weak var topView: LineRenderView!
    @IBOutlet weak var middleView: LineRenderView!
    @IBOutlet weak var bottomView: LineRenderView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func handleTap(_ sender: UITapGestureRecognizer) {
        topView.clearPoints()
    }
    
    @IBAction func handleGestureTopView(_ sender: UIPanGestureRecognizer) {
        topView.addPoint(toAdd: sender.location(in: topView))
        if sender.state == .ended{
            topView.startNewListOfPoints()
        }
    }
    
    @IBAction func handleGestureMiddleView(_ sender: UIPanGestureRecognizer) {
        if sender.state == .began{
            middleView.clearPoints()
            bottomView.clearPoints()
        }
        
        if sender.numberOfTouches == 1 {
            let current = sender.location(in: middleView)
            middleView.addPoint(toAdd: current)
            bottomView.addPoint(toAdd: current)
            
        }else if sender.numberOfTouches == 2 {
            let first = sender.location(ofTouch: 0, in: middleView)
            let second = sender.location(ofTouch: 1, in: middleView)
            middleView.addTwoTouchPoint(toAdd1: first, toAdd2: second)
            bottomView.addTwoTouchPoint(toAdd1: first, toAdd2: second)
        }
    }
    
    @IBAction func handleGestureBottomView(_ sender: UIPanGestureRecognizer) {
        if sender.state == .began{
            middleView.clearPoints()
            bottomView.clearPoints()
        }
        
        if sender.numberOfTouches == 1 {
            let current = sender.location(in: bottomView)
            middleView.addPoint(toAdd: current)
            bottomView.addPoint(toAdd: current)
            
        }else if sender.numberOfTouches == 2 {
            let first = sender.location(ofTouch: 0, in: bottomView)
            let second = sender.location(ofTouch: 1, in: bottomView)
            middleView.addTwoTouchPoint(toAdd1: first, toAdd2: second)
            bottomView.addTwoTouchPoint(toAdd1: first, toAdd2: second)
        }
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        topView.setNeedsDisplay()
        middleView.setNeedsDisplay()
        bottomView.setNeedsDisplay()
    }
}

