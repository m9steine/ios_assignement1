//
//  LineRenderView.swift
//  Assignement1_Layout
//
//  Created by Martin Steiner on 15.12.18.
//  Copyright © 2018 Martin Steiner. All rights reserved.
//

import UIKit

class LineRenderView: UIView {
    
    var points = [[CGPoint]]()
    
    func addPoint(toAdd: CGPoint) {
        
        if points.isEmpty{
            startNewListOfPoints()
        }
        points[points.count-1].append(toAdd)
        setNeedsDisplay()
    }
    
    func addTwoTouchPoint(toAdd1: CGPoint, toAdd2: CGPoint){
        
        if points.count < 2{
            startNewListOfPoints()
            startNewListOfPoints()
        }
        points[points.count-1].append(toAdd1)
        points[points.count-2].append(toAdd2)
        setNeedsDisplay()
    }
    
    func startNewListOfPoints() {
        points.append([CGPoint]())
    }
    
    override func draw(_ rect: CGRect) {
        if points.isEmpty{
            return
        }
        guard let context = UIGraphicsGetCurrentContext() else{
            return
        }
        //context.setStrokeColor(UIColor.black.cgColor)
        context.setLineWidth(3.0)
        
        for index in 0..<points.count{
            drawList(currentList: points[index], context: context)
        }
    }
    
    func drawList(currentList: [CGPoint], context: CGContext) {
        if currentList.isEmpty{
            return
        }
        context.move(to: currentList.first!)
        for index in 1..<currentList.count{
            context.addLine(to: currentList[index])
        }
        context.strokePath()
        
    }
    
    func clearPoints() {
        points.removeAll()
        setNeedsDisplay()
    }
    
    
    
}
